#!/bin/bash

if [ ! -f .env ]; then
    echo "Create Env File"
    cp .env.example .env
fi

if [ ! -d vendor ]; then
    echo "Installing Composer Dependencies"
    composer install --ignore-platform-reqs -n
fi

echo "Install sail with mysql"
php artisan sail:install --with mysql -n

echo "Run Initial Sail Up"
vendor/bin/sail up -d

echo "Install NPM Dependencies"
vendor/bin/sail npm install

echo "Generate App Key"
vendor/bin/sail artisan key:generate

echo "Add sleep so SQL can create databases within the container for next step"
sleep 20s

echo "Migrate Fresh Database"
vendor/bin/sail artisan migrate:fresh

echo "Start Npm Dev Server"
vendor/bin/sail npm run dev
