#!/bin/bash

set -e

echo "Running sail artisan test..."
./vendor/bin/sail artisan test

echo "Running pint..."
./vendor/bin/pint --test

echo "Running phpstan analyse..."
./vendor/bin/phpstan analyse --memory-limit=2G


# Done
echo "Script execution completed."
