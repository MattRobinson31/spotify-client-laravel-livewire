<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @vite('resources/css/app.css')
        <title>{{ $title ?? 'Page Title' }}</title>
    </head>
    <body class="bg-gray-100">
        <div class="bg-green-500 py-4 text-center">
            <h1 class="text-white text-2xl font-semibold">Spotify Client</h1>
        </div>
        {{ $slot }}
    </body>
</html>
