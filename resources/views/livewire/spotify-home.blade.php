<div class="container mx-auto mt-8 grid grid-cols-1 sm:grid-cols-2 gap-4">
    <livewire:artist-top-songs/>
    <div class="bg-white p-4 shadow rounded-lg min-h-5">
        <!-- Livewire component content for card 2 -->
    </div>
    <div class="bg-white p-4 shadow rounded-lg">
        <!-- Livewire component content for card 3 -->
    </div>
    <div class="bg-white p-4 shadow rounded-lg">
        <!-- Livewire component content for card 4 -->
    </div>
    <div class="bg-white p-4 shadow rounded-lg">
        <!-- Livewire component content for card 5 -->
    </div>
    <div class="bg-white p-4 shadow rounded-lg">
        <!-- Livewire component content for card 6 -->
    </div>
</div>
