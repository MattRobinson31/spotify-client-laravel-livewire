<div class="p-4 bg-green-400 shadow rounded-lg">
    <div class="mb-4">
        <h1 class="text-black font-bold mb-4">Search for your favourites artists top songs</h1>
        <input type="text" wire:model="artist" placeholder="Enter artist name"
               class="px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-400 w-full">
        <button wire:click="getTopSongs"
                class="mt-4 p-2 text-white bg-green-800 rounded-lg hover:bg-green-600
                    hover:text-black focus:outline-none focus:bg-green-600">
            Search
        </button>
    </div>

    @if ($topSongs)
        <ul>
            @foreach ($topSongs as $song)
                <li class="mb-2 text-gray-800">{{ $song['name'] }} - {{ $song['album']['name'] }}</li>
            @endforeach
        </ul>
    @endif
</div>
