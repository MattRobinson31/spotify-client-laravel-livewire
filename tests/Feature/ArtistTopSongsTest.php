<?php

use App\Livewire\ArtistTopSongs;
use App\ThirdParty\SpotifyApiInterface;
use Livewire\Livewire;

test('it can display top tracks for artist', function () {
    $spotifyApiMock = $this->mock(SpotifyApiInterface::class);

    $spotifyApiMock
        ->shouldReceive('searchArtist')
        ->once()
        ->andReturn("artist_id");

    $spotifyApiMock
        ->shouldReceive('getArtistTopTracks')
        ->once()
        ->with('artist_id')
        ->andReturn([
            [
                'artists' => [],
                'album' => [
                    'name' => 'Album 1'
                ],
                "name" => "Song 1",
            ],
            [
                'artists' => [],
                'album' => [
                    'name' => 'Album 2'
                ],
                "name" => "Song 2",
            ]
        ]);

    Livewire::test(ArtistTopSongs::class)
        ->call('boot', [$spotifyApiMock])
        ->set('artist', 'Artist Name')
        ->call('getTopSongs')
        ->assertSee('Song 1')
        ->assertSee('Song 2')
        ->assertSee('Album 1')
        ->assertSee('Album 2');
});
