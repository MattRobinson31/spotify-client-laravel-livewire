<?php

use App\ThirdParty\SpotifyApi;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;

test('successful search artist', function () {
    $mock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode([
            'access_token' => 'test123'
        ])),
        new Response(200, [
            'authorization' => 'Bearer test123'
        ], json_encode([
            'artists' => [
                'items' => [
                    [
                        'id' => 'ID123'
                    ]
                ]
            ]
        ])),
    ]);

    $handlerStack = HandlerStack::create($mock);
    $client = new Client(['handler' => $handlerStack]);
    $spotifyApi = new SpotifyApi($client, $this->createMock(LoggerInterface::class));

    $result = $spotifyApi->searchArtist('test mctester');

    expect($result)->toEqual('ID123');
});

test('successful top arist songs', function () {
    $mock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode([
            'access_token' => 'test123'
        ])),
        new Response(200, [
            'authorization' => 'Bearer test123'
        ], json_encode([
            'tracks' => [
                [
                    'artists' => [],
                    'albums' => [],
                    "disc_number" => 1,
                    "duration_ms" => 252306,
                    "explicit" => false,
                    "external_ids" => [],
                    "external_urls" => [],
                    "href" => "https://api.spotify.com/v1/tracks/4QNpBfC0zvjKqPJcyqBy9W",
                    "id" => "4QNpBfC0zvjKqPJcyqBy9W",
                    "is_playable" => true,
                    "name" => "Give Me Everything (feat. Ne-Yo, Afrojack & Nayer)",
                    "popularity" => 85,
                    "preview_url" => "https://p.scdn.co/mp3-preview/6eafa4293d2b35b2e75ffab5ec1bba8ec00d5082?cid=ddf5a74d2dc8421291911e9281769738",
                    "track_number" => 2,
                    "type" => "track",
                    "uri" => "spotify:track:4QNpBfC0zvjKqPJcyqBy9W",
                    "is_local" =>false
                ],
                [
                    'artists' => [],
                    'albums' => [],
                    "disc_number" => 1,
                    "duration_ms" => 252306,
                    "explicit" => false,
                    "external_ids" => [],
                    "external_urls" => [],
                    "href" => "https://api.spotify.com/v1/tracks/4QNpBfC0zvjKqPJcyqBy9W",
                    "id" => "4QNpBfC0zvjKqPJcyqBy9W",
                    "is_playable" => true,
                    "name" => "Give Me Everything (feat. Ne-Yo, Afrojack & Nayer) Number 2",
                    "popularity" => 85,
                    "preview_url" => "https://p.scdn.co/mp3-preview/6eafa4293d2b35b2e75ffab5ec1bba8ec00d5082?cid=ddf5a74d2dc8421291911e9281769738",
                    "track_number" => 2,
                    "type" => "track",
                    "uri" => "spotify:track:4QNpBfC0zvjKqPJcyqBy9W",
                    "is_local" =>false
                ]
            ]
        ]))
    ]);

    $handlerStack = HandlerStack::create($mock);
    $client = new Client(['handler' => $handlerStack]);
    $spotifyApi = new SpotifyApi($client, $this->createMock(LoggerInterface::class));

    $result = $spotifyApi->getArtistTopTracks('ID123');

    expect($result)->toHaveCount(2)
        ->and($result[0]['name'])->toEqual("Give Me Everything (feat. Ne-Yo, Afrojack & Nayer)")
        ->and($result[1]['name'])->toEqual("Give Me Everything (feat. Ne-Yo, Afrojack & Nayer) Number 2");
});
