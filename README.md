![Spotify :pgp](https://storage.googleapis.com/pr-newsroom-wp/1/2018/11/Spotify_Logo_RGB_Green.png)

Quick Example of writing unit & feature tests for a coding challenge found here: https://codingchallenges.substack.com/p/coding-challenge-18-spotify-client

Nowhere near complete but current use:

# Setup
1. Run `./install.sh`
2. Add shopify dev app key & secret too env file

# Use
**Go to** `localhost/spotify`

# Testing
Run `./local-test.sh`
