<?php

namespace App\Providers;

use App\ThirdParty\SpotifyApi;
use App\ThirdParty\SpotifyApiInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(SpotifyApiInterface::class, SpotifyApi::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
