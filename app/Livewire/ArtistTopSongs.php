<?php

namespace App\Livewire;

use App\ThirdParty\SpotifyApiInterface;
use Livewire\Component;

class ArtistTopSongs extends Component
{
    public string $artist;
    public array $topSongs;
    private SpotifyApiInterface $api;

    public function boot(SpotifyApiInterface $spotifyApi): void
    {
        $this->api = $spotifyApi;
    }

    public function getTopSongs(): void
    {
        $artistId = $this->api->searchArtist($this->artist);
        $this->topSongs = $this->api->getArtistTopTracks($artistId);
    }
}
