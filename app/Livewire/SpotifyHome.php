<?php

namespace App\Livewire;

use Illuminate\Contracts\View\View;
use Livewire\Component;

class SpotifyHome extends Component
{
    public string $title = 'Home';

    public function mount(): void
    {
        $this->title = 'Home';
    }

    public function render(): View
    {
        return view('livewire.spotify-home')->layoutData(['title' => 'Spotify Client Homepage']);
    }
}
