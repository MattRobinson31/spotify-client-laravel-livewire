<?php

namespace App\ThirdParty;

interface SpotifyApiInterface
{
    public function searchArtist(string $artistName): string;
    public function getArtistTopTracks(string $artistId): array;
}
