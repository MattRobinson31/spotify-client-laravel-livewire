<?php

namespace App\ThirdParty;

use App\Exceptions\SpotifyApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Psr\Log\LoggerInterface;

class SpotifyApi implements SpotifyApiInterface
{
    private string $accessToken;

    public function __construct(private readonly Client $client, private readonly LoggerInterface $logger)
    {
        /** @todo Fix cache
         * Cache::get('SpotifyAccessToken', '', now()->addMinutes(5)) ?:
         */

        $this->accessToken = $this->getAccessToken();
    }

    public function searchArtist(string $artistName): string
    {
        $formatArtistName = str_replace(' ', '+', $artistName);

        try {
            $response = $this->client->get(
                'https://api.spotify.com/v1/search/?market_us&type=artist&q=artist' . $formatArtistName,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->accessToken
                    ]
                ]
            );

            $result = json_decode($response->getBody(), true);

            return $result['artists']['items'][0]['id'];
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            throw new SpotifyApiException($e->getMessage());
        }
    }

    public function getArtistTopTracks(string $artistId): array
    {
        try {
            $response = $this->client->get(
                "https://api.spotify.com/v1/artists/{$artistId}/top-tracks?market=US",
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->accessToken
                    ]
                ]
            );

            $result = json_decode($response->getBody(), true);

            return $result['tracks'];
        } catch (GuzzleException $e) {
            $this->logger->error($e->getMessage());
            throw new SpotifyApiException($e->getMessage());
        }
    }

    private function getAccessToken(): string
    {
        try {
            $response = $this->client->post("https://accounts.spotify.com/api/token", [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => env('SHOPIFY_API_KEY'),
                    'client_secret' => env('SHOPIFY_API_SECRET')
                ]
            ]);

            $result = json_decode($response->getBody(), true);

            return $result['access_token'];
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new SpotifyApiException($e->getMessage());
        }
    }
}
